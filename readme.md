This is a project which follows Stephen Grider's "Typescript: The Complete Developer's Guide [2020]" course on udemy.com

This project is about design patterns using typescript, and how to create reuseable code by using certain design patterns in typescript.
