// T refers to the type that will be passed into the class for further reference
class ArrayOfAnything<T> {
  constructor(public collection: T[]) {}

  get(index: number): T {
    return this.collection[index];
  }
}

// Instantiate an Array of strings (instead of anything in this case). Now the 'collection' variable is an array of strings.
new ArrayOfAnything<string>(['a', 'b', 'c', 'd']);

// Instantiate an Array of numbers (instead of anything in this case). Now the 'collection' variable is an array of numbers.
new ArrayOfAnything<number>([1, 2, 3, 4, 5]);

// Generics with functions

// Code duplication
function printStrings(array: string[]): void {
  for (let i = 0; i < array.length; i++) {
    console.log(array[i]);
  }
}

// Code duplication
function printNumbers(array: number[]): void {
  for (let i = 0; i < array.length; i++) {
    console.log(array[i]);
  }
}

// Solve code duplication with generics. There's no need to create different function signatures with different parameter types,
// but with the same body.
function printAnything<T>(array: T[]): void {
  for (let i = 0; i < array.length; i++) {
    console.log(array[i]);
  }
}

// Implicit type declaration.
printAnything<string>(['a', 'b', 'z', 'x']);
// Type inference occurs here. Due to the parameter passed in, Typescript infers that <T> will be a <string>
printAnything(['a', 'b', 'z', 'x']);

// It's still be to pass in a type annotation, to prevent small errors such as by accident passing an array of numbers, when strings meant to be passed in as below
printAnything([1, 2, 3, 4, 5]);

// Generic Constraints

class Car {
  print() {
    console.log('This is a car.');
  }
}

class House {
  print() {
    console.log('This is a house.');
  }
}

interface Printable {
  print(): void;
}

// The print() method does not exist on an unknown type T.
// Generic constraints need to be added, in order to promise that a print() method will be present.
// The T type will extend the Printable interface, so ONLY objects that implement the Printable interface can be passed into this interface
function printHousesOrCars<T extends Printable>(array: T[]): void {
  for (let index = 0; index < array.length; index++) {
    array[index].print();
  }
}

// The three below examples will work, since they all have an array of objects that satisfy the Printable interface.
printHousesOrCars<House>([new House(), new House()]);
printHousesOrCars<Car>([new Car(), new Car()]);
printHousesOrCars<Printable>([new Car(), new House()]);
// This is not correct, since the number type does not implement a print method. An error will be shown below.
printHousesOrCars([1, 2, 3, 4]);
